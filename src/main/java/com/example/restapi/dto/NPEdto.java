package com.example.restapi.dto;

/*
* This is the null pointer exception DTO for Controller advice
* */
public class NPEdto {
    private String message;
    private String type;

    public NPEdto() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
