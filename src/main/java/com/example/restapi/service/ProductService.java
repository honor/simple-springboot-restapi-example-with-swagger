package com.example.restapi.service;

import com.example.restapi.model.product;
import com.example.restapi.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {//this service will serve all the functionality of the ProductRepository

    private ProductRepository productRepository;

    @Autowired
    public void productRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * We could also add logging for entering and leaving the methods using Aspects (AOP)
     */
    private Logger LOG = LoggerFactory.getLogger(ProductService.class);

    public product getProduct(String id){
        LOG.info("Getting the product with id: " + id);
        return productRepository.findOne(id);
    }

    public product saveProduct(product p){
        product productSaved;
        try {
            LOG.info("saving product...");
            productSaved = productRepository.save(p);
            return productSaved;
        }
        catch (Exception e){
            LOG.error("An error occurred while trying to save the product: " + e.getMessage());
        }
        return new product();
    }

    public product updateProduct(product productToUpdate, String id){
        product foundProduct = productRepository.findOne(id);
        try {
            foundProduct.setName(productToUpdate.getName());
            foundProduct.setDescription(productToUpdate.getDescription());
            foundProduct.setType(productToUpdate.getType());
            foundProduct.setCategory(productToUpdate.getCategory());
            return productRepository.save(foundProduct);
        }catch (Exception e){
            LOG.error("An error accorred during update..." + e.getMessage());
        }
        return productToUpdate;
    }
    public void deleteProduct(String id){
        try {
            productRepository.delete(id);
        }
        catch (Exception e){
            LOG.error("An error occurred while deleting produduct..." + e.getMessage());
        }
    }
}
