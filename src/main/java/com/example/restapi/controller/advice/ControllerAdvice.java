package com.example.restapi.controller.advice;

import com.example.restapi.dto.NPEdto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {//here we handle all the 'nullpointerexception's for "all"? controllers..

    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public NPEdto processNPE(NullPointerException exception){
        NPEdto npe = new NPEdto();
        npe.setMessage("There were problems with the request. Try again later.");
        npe.setType("ERROR");
        return npe;
    }
}
