package com.example.restapi.controller;

import com.example.restapi.model.product;
import com.example.restapi.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/api/products/")
public class ProductController {

    private Logger LOG = LoggerFactory.getLogger(ProductController.class);
    private ProductService productService;

    @Autowired
    public void productService(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(path="{id}", method= RequestMethod.GET)
    @ApiOperation("Returns a Product with a specific ID")
    public product getProduct(@PathVariable(name = "id") String id){
        return productService.getProduct(id);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Records a new Product")
    public product saveProduct(@RequestBody product productToSave){
        return productService.saveProduct(productToSave);
    }

    @RequestMapping(path="{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Updates a Product with a specific ID")
    public product updateProduct(@RequestBody product productToUpdate, @PathVariable(name="id") String id){
        return productService.updateProduct(productToUpdate, id);
    }

    @RequestMapping(path="{id}", method = RequestMethod.DELETE)
    @ApiOperation("Deletes a Product with a specific ID")
    public void deleteProduct(@PathVariable(name="id") String id){
        productService.deleteProduct(id);
    }
}
