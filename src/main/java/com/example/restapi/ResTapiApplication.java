package com.example.restapi;

import com.example.restapi.model.product;
import com.example.restapi.repository.ProductRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResTapiApplication implements CommandLineRunner{

	org.slf4j.Logger LOG = LoggerFactory.getLogger(ResTapiApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ResTapiApplication.class, args);
	}

	private ProductRepository productRepository;
	@Autowired
	public void productRepository(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public void run(String... strings) throws Exception {
		product testProduct = new product();
		testProduct.setName("simple product");
		testProduct.setDescription("this is a tester product");
		testProduct.setType("CUSTOM");
		testProduct.setCategory("special");

		productRepository.save(testProduct);
		LOG.info("CommandLineRunner's run method completed");
	}
}
